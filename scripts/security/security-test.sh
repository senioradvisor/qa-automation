#!/bin/bash
echo "Starting Zap Container"
sh ./scripts/security/start-zap.sh
sleep 2
echo "Running Selenium Tests"
yarn run security
sleep 5
echo "Generating Report & Removing the Container"
sh ./scripts/security/stop-zap.sh
