container=`cat ./scripts/security/container.txt`

docker exec $container zap-cli report -o vulnerability.html -f html

docker cp $container:/zap/vulnerability.html ./reports/vulnerability/vulnerability.html

sleep 5

docker stop $container
docker rm $container