container="$(docker run -u zap -p 8080:8080 -d owasp/zap2docker-weekly zap.sh -daemon -host 0.0.0.0 -port 8080 -config api.disablekey=true -config api.addrs.addr.name=.* -config api.addrs.addr.regex=true)"

sleep 8

docker exec $container zap-cli status
docker exec $container zap-cli session new

echo $container > "./scripts/security/container.txt"
