import Page from './page';


class AssistedLivingPage extends Page {

  //Define Elements
  get assistedlivingBtn() {
    const [, assistedlivingBtnSelector] = $$('.parent_menu > li a');
    return assistedlivingBtnSelector;
  }

  get mobileMenu() { return $('.mobile-menu-btn'); }

  //Define or overwrite page methods

  appendOpen() {
    this.open('');
    browser.pause(1000);
  }
  goToAssistedLiving() {
    browser.waitForExist('.parent_menu > li a');
    this.assistedlivingBtn.click();
  }

  doubleTapAssistedLiving() {
    browser.waitForExist(this.assistedlivingBtn.selector);

    this.assistedlivingBtn().doubleClick();
  }

  mobileAssistedliving() {
    this.mobileMenu.click();
  }



}

export default AssistedLivingPage;
