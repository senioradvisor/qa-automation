import axios from 'axios';
import states from '../config/states.json';
import {assert} from 'chai';

export default class Page {
  //get countCities(){ return browser.elements('.cities a');}
  get countCities(){ return browser.elements('.dma-container a');}

  get countStates(){ return browser.elements('.state-container a');}
  
  get citiesLinks() { return $$('.dma-container a'); }
  //get citiesLinks() { return $$('.cities a'); }  
  get statesLinks() { return $$('.state-container a'); }

  open(path) {
    //Baseurl equals path  
    browser.setViewportSize({ width: 1920, height: 1080 });
    browser.url(path);
  }

  clickandValidateLinks(page, text) {
    browser.pause(1000);
    if (text === 'states') {
      let i = 0;

      while (i < this.countStates.value.length) {
        let state = this.statesLinks[i].getText().replace(new RegExp(' ', 'g'), '-').trim().toLowerCase();
        let url = this.statesLinks[i].getAttribute('href');
        let finalURL = state === 'district-of-columbia' ? `${process.env.BASE_URL}/${page}/${state}/washington` : `${process.env.BASE_URL}/${page}/${state}`;

        this.statesLinks[i].click();
        browser.pause(1000);
        let redirectUrl = browser.getUrl();
        
        assert.equal(url, redirectUrl, `Assertion Error: URL does not match the button name, it was ${redirectUrl} and it must be ${url}`);
        assert.equal(redirectUrl, finalURL, `Assertion Error: URL does not match the button name, it was ${redirectUrl} and it must be ${url}`);
        
        axios.get(url).then(function (response) {
          assert.equal(200, response.status, `Assertion Error: The response status was not 200 OK on ${url}`);
        });
        

        browser.back();
        
        i++;
      }
    } else if (text === 'cities') {
      let i = 0;

      while (i < this.countCities.value.length) { 
        let [city,state] = this.citiesLinks[i].getText().split(', ').map(item => item.replace(new RegExp(' ', 'g'), '-').trim());
        let url = this.citiesLinks[i].getAttribute('href');
        let finalURL = city === 'Washington-DC' ? `${process.env.BASE_URL}/${page}/district-of-columbia/washington` : `${process.env.BASE_URL}/${page}/${this.buildURL(state)}/${city.toLowerCase()}`;

        this.citiesLinks[i].click();
        
        let redirectUrl = browser.getUrl();
        
        assert.equal(url, redirectUrl, `Assertion Error: URL does not match the button name, it was ${redirectUrl} and it must be ${url}`);
        assert.equal(redirectUrl, finalURL, `Assertion Error: URL does not match the button name, it was ${redirectUrl} and it must be ${url}`);
        
        axios.get(url).then(function (response) {
          assert.equal(200, response.status, `Assertion Error: The response status was not 200 OK on ${url}`);
        });  
        
        browser.back();
        
        i++;

      }
    }
  }

  buildURL(state='NM'){
    return states[state].replace(new RegExp(' ', 'g'), '-').toLowerCase();
  }

  findElementinTable(text,element,position){
    let i = 0;
    let table = element === '' ? $('tbody') : $(`tbody${element}`);

    while (i < $$(`${table.selector} tr`).length) {
      let valuetemp=$$(`${table.selector} tr`)[i].$$('td')[position];  
      if(valuetemp.getText() === text){
        return valuetemp;
      }
      i++;
    }
  }
}