import puppeteer from 'puppeteer';
import { toMatchImageSnapshot } from 'jest-image-snapshot';

expect.extend({ toMatchImageSnapshot });

it('should access APFM and validate layout for assisted-living', async () => {

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setViewport({ width: 1920, height: 1080 });
  await page.goto('https://staging.www.aplaceformom.com/assisted-living');
  const image = await page.screenshot();
  await browser.close();

  expect(image).toMatchImageSnapshot();

});
