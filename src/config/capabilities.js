var browserstack = require('browserstack-local');
const path = require('path');
const local = new browserstack.Local();
const dotenv = require('dotenv');

const webMetadata = { 
  metadata: { 
    device: 'MacBook Pro', 
    platform: { 
      name: 'OSX', 
      version: '10.13.6' 
    }
  } 
};

const iOSMetadata = { 
  metadata:  { 
    device: 'IPHONE X', 
    platform: { 
      name: 'iOS',
      version: '11.4'
    }
  }
};

const androidMetadata = { 
  metadata: {
    device: 'Nexus 5', 
    platform: { 
      name: 'Android',
      version: '8.0'
    }
  }
};

const proxy = 'http://localhost:8080';

const proxySettings = {
  proxy: {
    httpProxy: proxy,
    sslProxy: proxy,
    ftpProxy: proxy,
    proxyType: 'MANUAL',
    autodetect: false
  },
  'chrome.switches': [
    '--ignore-certificate-errors'
  ]
};

dotenv.config();

module.exports = {

  ios: () => ({
    capabilities: [{
      browserName: 'safari',
      commandTimeout: '7200',
      platformName: 'iOS',
      platformVersion: '11.4.1',
      deviceName: 'iPhone X',
      udid: '172AC833-B2E9-4362-91BD-70AC9EF88B1E',
      bundleId: 'com.bytearc.SafariLauncher',
      cssSelectorsEnabled: true,
      nativeWebTap: true,
      nativeEvents: true,
      autoWebView: true,

      ...iOSMetadata
    }],

    specs: [path.join(__dirname, '..', 'features', process.env.PROJECT , 'mobile', '*.feature')],

    services: ['appium'],
    appium: {
      args: {
        sessionOverride: true,
        address: '127.0.0.1',
        port: '4444',
      }
    }

  }),

  android: () => ({
    capabilities: [{
      browserName: 'chrome',
      commandTimeout: '7200',
      automationName: 'uiAutomator2',
      platformName: 'Android',
      platformVersion: '7.0',
      deviceName: 'NXS_5',
      avd: 'NXS_5',

      ...androidMetadata
    }],

    specs: [path.join(__dirname, '..', 'features', process.env.PROJECT, 'mobile', '*.feature')],

    services: ['appium'],
    
    appium: {
      args: {
        sessionOverride: true,
        address: '127.0.0.1',
        port: '4444',
      }
    }
  }),

  web: (enableProxy) => { 
    let finalProxy = enableProxy ? proxySettings : {};

    return { 
      capabilities: [
        { browserName: process.env.BROWSER, ...webMetadata, ...finalProxy }, 
        // { browserName: 'firefox', ...webMetadata }
        // ,{ browserName: 'safari', ...webMetadata }
      ],
      specs: [path.join(__dirname, '..', 'features', process.env.PROJECT, 'web', '*.feature')],
      services: ['selenium-standalone']
    };
  },

  browserstack: () => ({
    services: ['browserstack'],
    user: 'oscarazofeifa1',
    key: '2U9vqsDuySxjRhdZUpEs',
    capabilities: [{
      'browserstackLocal': true,
      'os': 'Windows',
      'os_version': '10',
      'browser': 'Edge',
      'browser_version': '17.0',
      'resolution': '1920x1080',
      'build': 'test-version1.1',
      'project': 'Assisted-Living Sample',
    }],


    onPrepare: (config, _capabilities) => {
      return new Promise(function (resolve, reject) {
        local.start({ 'key': config.key }, function (error) {
          if (error) return reject(error);
          resolve();
        });
      });
    },

    onComplete: (_capabilties, _specs)  => {
      process.kill(local.pid);
    },

    specs: [path.join(__dirname, '..', 'features', process.env.PROJECT, 'web', '*.feature')],

  })

};