import { Given } from 'cucumber';

const BASE_URL = process.env.BASE_URL;

Given('I go on the {string} page', (relativeURL) => {
  browser.setViewportSize({ width: 1920, height: 1080 });
  return relativeURL === 'home' ? browser.url(`${BASE_URL}/`) : browser.url(`${BASE_URL}/${relativeURL}`);
});

Given('I go on the device to {string}', (relativeURL) => {
  browser.url(`${BASE_URL}/${relativeURL}`);
});