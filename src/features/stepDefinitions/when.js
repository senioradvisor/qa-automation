import { When } from 'cucumber';

import assistedLiving from '../../pageObjects/assistedLiving.page';
const assisted = new assistedLiving();

When('I will click the button on the nav-bar for assisted-living', () => {
  assisted.goToAssistedLiving();
});

When('I click the button {string} in the section {string}', (button, _section) => {
  browser.click(button);
});

When('I double-click the button {string} in the section {string}', (button, _section) => {
  browser.doubleClick(button);
});

When('I scroll to {string} in the section {string}', (element, _section) => {
  browser.scroll(element);
});


When('I type {string} in the element {string}', (text, element) => {
  browser.setValue(element, text);
});

When('I press the key {string}', key => {
  browser.keys(key);
});

When('I click the back button', () => {
  browser.back();
});

When('I click the forward button', () => {
  browser.forward();
});

When('I take a screenshot', () =>{
  browser.saveScreenshot();
});

When('I select the option {string} in the element {string} on ListView {string}' , (option,element,_listview) => {
  browser.selectByValue(element.option);
});

When('I wait {string} seconds', (seconds) => {
  browser.pause(seconds*1000);
});

When('I click {string} in the selector {string} in the position {string}' , (text,element,position) => {
  let webElement= assisted.findElementinTable(text,element,position);
  webElement.waitForExist();
  webElement.click();
});

When('I click the element with text {string} in the position {string}', (text, position) => {
  let webElement = $$(`[value="${text}"]`)[position];
  webElement.waitForExist();
  webElement.click();

});


When('I will tap the menu button', () => {
  assisted.mobileAssistedliving();
});

When('I will double tap the Assisted Living button', () => {
  assisted.goToAssistedLiving();
});

