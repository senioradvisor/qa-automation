import {assert} from 'chai';
import { Then } from 'cucumber';

import assistedLiving from '../../pageObjects/assistedLiving.page';

const assisted = new assistedLiving();
const BASE_URL = process.env.BASE_URL;

Then('should the title of the page be {string}', expectedTitle => {
  const title = browser.getTitle();
  assert.equal(title, expectedTitle, `title is ${title} but should be ${expectedTitle}`);
});

Then('should the url of the page be {string}', (relativeURL) => {
  browser.waitUntil( () => browser.execute('return document.readyState').value === 'complete');
  const url = browser.getUrl();
  let finalURL = relativeURL === 'home' ? `${BASE_URL}/` : `${BASE_URL}/${relativeURL}`;
  assert.equal(url, finalURL, `URL is ${url} but should be ${BASE_URL}/${relativeURL}`);
});

Then('should the element {string} contains {string}', (element, text) => {
  assert.equal(browser.element(element).getText(), text);
});

Then('should the element {string} do not contain {string}', (element, text) => {
  assert.notEqual(browser.element(element).getText(), text);
});

Then('should the element {string} is displayed', (element) => {
  assert.equal(browser.element(element).isVisible(), true);
});

Then('should the element {string} not be displayed', (element) => {
  assert.equal(browser.isVisible(element), false);
});

Then('should the element {string} be selected', (element) => {
  assert.equal(browser.isSelected(element), true);
});

Then('should the element {string} not be selected', (element) => {
  assert.equal(browser.isSelected(element), false);
});

Then('should the element {string} be disabled', (element) => {
  assert.equal(browser.isEnabled(element), false);
});

Then('should the element {string} be enabled', (element) => {
  assert.equal(browser.isEnabled(element), true);
});

Then('should the count of the element {string} will be {string}', (element, finalCount) => {
  let count = browser.element(element).value.length();
  assert.equal(count.toString(), finalCount);
});

Then('I will click {string} in {string} links and validate redirection and status code', (text, page) => {
  assisted.clickandValidateLinks(page, text);
});

