Feature: Page Redirects
    This feature file was created with the intention to validate the page redirects
    are being properly handled. This is not for an specific page or site.

    Scenario Outline: Validate the destination page is equal to the new URL provided
        Given I go on the page "<OLD>"
        Then  should the url of the page be "<NEW>"

    Examples:
    | OLD | NEW |
    | /planning-and-advice/articles/elderly-urinary-tract-infections | articles/elderly-urinary-tract-infection |
    | /planning-and-advice/parkinsons-disease-in-the-elderly | articles/parkinsons-disease-in-the-elderly |
    | /planning-and-advice/articles/eating-alone | articles/eating-alone |
    | /planning-and-advice/articles/dementia-information | articles/dementia-information |
    | /planning-and-advice/articles/moving-elderly-parents | articles/moving-elderly-parents |
    | /planning-and-advice/articles/hip-fractures-in-the-elderly | articles/hip-fractures-in-the-elderly |
    | /planning-and-advice/articles/importance-of-taking-medications | articles/importance-of-taking-medications |
    | /senior-care-resources/articles/benefits-for-veterans-and-their-spouses | planning-and-advice/senior-housing-101/veterans-benefits |
    | /senior-care-resources/articles/health | planning-and-advice/articles |
    | /senior-care-resources/articles/adult-day-services | planning-and-advice/articles/adult-day-services |
    | /senior-care-resources/articles/tough-conversations | planning-and-advice/articles/elder-care-planning |