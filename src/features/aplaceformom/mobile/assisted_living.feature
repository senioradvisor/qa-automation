Feature: Example feature
  As a user of Aplaceformom.com
  I should be able to browse to Assisted living page
  to get assert the migration

  Scenario: Get title of website
    Given I go on the device to "assisted-living"
    Then  should the title of the page be "What is Assisted Living? - How Can I Find Assisted Living Near Me?"

  Scenario: Test Routing for assisted-living
   Given I go on the device to "https://www.aplaceformom.com/"
   And I will tap the menu button
   And I will double tap the Assisted Living button
   Then should the url of the page be "https://www.aplaceformom.com/assisted-living"