const { environment, proxy } = require('yargs').argv;
const config = require('./src/config/capabilities.js')[environment || 'web'];
const path = require('path');
const dotenv = require('dotenv');
const WdioCucumberTestRailReporter = require('wdio-testrail-cucumber-reporter');

dotenv.config();

const finalConfig = typeof config === 'function' ? config(proxy) : config;

exports.config = {

  ...finalConfig,

  // ==================
  // Specify Test Files
  // ==================

  // Patterns to exclude.
  exclude: [
    // 'path/to/excluded/files'
  ],


  maxInstances: 1,

  sync: true,

  // Level of logging verbosity: silent | verbose | command | data | result | error
  logLevel: 'silent',

  //

  baseUrl: process.env.BASE_URL,

  screenshotPath: './screenshots/',

  // Enables colors for log output.

  coloredLogs: true,
  //
  // Warns when a deprecated command is used

  deprecationWarnings: true,
  //
  // If you only want to run your tests until a specific amount of tests have failed use
  // bail (default is 0 - don't bail, run all tests).

  bail: 0,

  // Default timeout for all waitFor* commands.
  waitforTimeout: 10000,
  //
  // Default timeout in milliseconds for request
  // if Selenium Grid doesn't send response
  connectionRetryTimeout: 90000,
  //
  // Default request retries count
  connectionRetryCount: 3,


  framework: 'cucumber',

  reporters: ['spec', 'multiple-cucumber-html', WdioCucumberTestRailReporter],
  testRailsOptions: {
    domain: 'testapfm.testrail.io',
    username: 'oscar.azofeifa@aplaceformom.com',
    password: 'Password1.',
    projectId: 6,
    suiteId: 9,
    runName: process.env.REPORT_NAME,
    includeAll: true
  },
  reporterOptions: {
    htmlReporter: {
      jsonFolder: './reports/cucumber',
      reportFolder: './reports/cucumberReport',
      removeFolders: true,
      displayDuration: true,
      // ... other options, see Options
    },
  },
  //


  cucumberOpts: {
    require: [path.join(__dirname, 'src/features/stepDefinitions', 'given.js'), path.join(__dirname, 'src/features/stepDefinitions', 'when.js'), path.join(__dirname, 'src/features/stepDefinitions', 'then.js')],
    backtrace: false,   // <boolean> show full backtrace for errors
    compiler: ['js:babel-register'],       // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    dryRun: false,      // <boolean> invoke formatters without executing steps
    failFast: false,    // <boolean> abort the run on first failure
    format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    colors: true,       // <boolean> disable colors in formatter output
    snippets: true,     // <boolean> hide step definition snippets for pending steps
    source: true,       // <boolean> hide source uris
    profile: [],        // <string[]> (name) specify the profile to use
    strict: true,      // <boolean> fail if there are any undefined or pending steps
    tags: ['@adrian'],           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    timeout: 99999999,     // <number> timeout for step definitions
    ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings.
  },

  //
  // =====
  // Hooks
  // =====
  // WebdriverIO provides several hooks you can use to interfere with the test process in order to enhance
  // it and to build services around it. You can either apply a single function or an array of
  // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got
  // resolved to continue.
  /**
     * Gets executed once before all workers get launched.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
  // onPrepare: function (config, capabilities) {
  // },
  /**
     * Gets executed just before initialising the webdriver session and test framework. It allows you
     * to manipulate configurations depending on the capability or spec.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
  // beforeSession: function (config, capabilities, specs) {
  // },
  /**
     * Gets executed before test execution begins. At this point you can access to all global
     * variables like `browser`. It is the perfect place to define custom commands.
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
  before: function (_capabilities, _specs) {
    require('babel-register');
  },
  /**
     * Runs before a WebdriverIO command gets executed.
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     */
  // beforeCommand: function (commandName, args) {
  // },

  /**
     * Hook that gets executed before the suite starts
     * @param {Object} suite suite details
     */
  // beforeSuite: function (suite) {
  // },
  /**
     * Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
     * @param {Object} test test details
     */
  // beforeTest: function (test) {
  // },
  /**
     * Hook that gets executed _before_ a hook within the suite starts (e.g. runs before calling
     * beforeEach in Mocha)
     */
  // beforeHook: function () {
  // },
  /**
     * Hook that gets executed _after_ a hook within the suite ends (e.g. runs after calling
     * afterEach in Mocha)
     */
  // afterHook: function () {
  // },
  /**
     * Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) ends.
     * @param {Object} test test details
     */
  // afterTest: function (test) {
  // },
  /**
     * Hook that gets executed after the suite has ended
     * @param {Object} suite suite details
     */
  // afterSuite: function (suite) {
  // },

  /**
     * Runs after a WebdriverIO command gets executed
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     * @param {Number} result 0 - command success, 1 - command error
     * @param {Object} error error object if any
     */
  // afterCommand: function (commandName, args, result, error) {
  // },
  /**
     * Gets executed after all tests are done. You still have access to all global variables from
     * the test.
     * @param {Number} result 0 - test pass, 1 - test fail
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
  // after: function (result, capabilities, specs) {
  // },
  /**
     * Gets executed right after terminating the webdriver session.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
  // afterSession: function (config, capabilities, specs) {
  // },
  /**
     * Gets executed after all workers got shut down and the process is about to exit.
     * @param {Object} exitCode 0 - success, 1 - fail
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
  // onComplete: function(exitCode, config, capabilities) {
  // }
};
